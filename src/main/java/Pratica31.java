
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jo�o Paulo
 */
public class Pratica31 {
    
        private static Date inicio, fim;
        private static String meuNome = "joao PAulo STADLER";
        private static GregorianCalendar dataNascimento = new GregorianCalendar(1991, Calendar.JULY, 4);
        private static long dias;
    
        public static void main(String[] args) {
        
        inicio = new Date();
             
        System.out.println(meuNome.toUpperCase());
        
        meuNome = meuNome.substring(11,12).toUpperCase()+meuNome.substring(12).toLowerCase()+", "+meuNome.substring(0,1).toUpperCase()+". "+meuNome.substring(5,6).toUpperCase()+".";
        System.out.println(meuNome);
                       
        dias = (inicio.getTime()-dataNascimento.getTime().getTime())/86400000;
        System.out.println(dias);
        
        fim = new Date();
        System.out.println(fim.getTime()-inicio.getTime());
    }
}
